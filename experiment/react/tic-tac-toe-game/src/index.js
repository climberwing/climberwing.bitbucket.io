﻿import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

//Create Square Component: controlled components
// class Square extends React.Component{
//     // constructor(props){
//     //     super(props)
//     //     this.state = {
//     //         value: null
//     //     }
//     // }
//     render() {
//         return (
//             //Create a Button DOM
//             <button className="square" 
//                     // onClick={()=>this.setState({value:'X'})}>
//                     onClick={()=>this.props.onClick()}>
//                    {this.props.value}
//                     {/* {this.state.value}   */}
//             </button>
//         )
//     }
// } 
//Square fundtion component
var Square = props => {
  return (
    <button className="square"
      onClick={props.onClick}
    >
      {props.value}
    </button>
  )
}

var calculateWinner = squares => {
  //winning pattern
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ]
  for (let i of lines) {
    const [a, b, c] = i
    // console.log(i)
    // console.log(squares[a])
    //console.log(squares[b])
    //console.log(squares[c])
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) //winning logic
      return squares[a]
  }
  return null
}

//Broad Component 
//Constructor: Broad | Prototype: React.Component
class Board extends React.Component {
  // constructor(props) {
  //   super(props)
  //   //Set state in the board
  //   this.state = {
  //     squares: Array(9).fill(null),
  //     xIsNext: true,
  //   }
  // }
  //Render Square Square Component
  renderSquare (i) {
    // return <Square value={i} /> //passed from Square Component 
    // return <Square value={this.state.squares[i]}
    //   onClick={() => this.handleClick(i)} /> //return square array[i]
    return <Square value={this.props.squares[i]}
      onClick={() => this.props.onClick(i)} /> //return square array[i]
  }



  render () {

    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}



//Broad Game Component
class Game extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
    }
  }
  handleClick (i) {
    // const history = this.state.history
    const history = this.state.history.slice(0, this.state.stepNumber + 1)
    const current = history[history.length - 1]
    // const squares = this.state.squares.slice(); //copy array
    const squares = current.squares.slice();
    // console.log(squares[i])
    if (calculateWinner(squares) || squares[i]) {
      // console.log('squares[i]')
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O'    //set value X/O
    // console.log(squares[i])

    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      // squares: squares,
      xIsNext: !this.state.xIsNext,
    })   //set state
  }
  jumpTo = step => {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }
  render () {
    const history = this.state.history
    const current = history[this.state.stepNumber]
    const winner = calculateWinner(current.squares)

    const moves = history.map((step, move) => {//currentValue, index
      const desc = move ? `Go to move #${move}` : `Go to game start`
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      )
    }

    )

    let status
    if (winner) {
      status = `Winner: ${winner}`
    }
    else {
      status = `Next player: ${this.state.xIsNext ? 'X' : 'O'}`
    }
    return (
      <div className="game">
        <div className="game-board">
          <Board squares={current.squares} onClick={i => this.handleClick(i)} />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================
//Render Broad Game Component to Page
ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
